Sister City's AI Tool

This code uses Microsoft Custom Vision API to find objects and phenomena in the skyline and report such findings to Ableton Live using the OSC protocol.

This repo contains code to do the following:

    Take pictures using Sister City's Skycam. 
    Upload pictures to Azure's Custom Vision API for analysis.
    Report objects/phenomena identified by the API in the pictures to Ableton Live via OSC.

Specifications and Dependencies

    This code is running on Windows 10.
    Requires Microsoft Developer account access.

Step-by-Step Get code running

    1. Install Python 3 (https://www.python.org/downloads/) and VLC Player (https://www.videolan.org/vlc/). Make sure both installers are compatible with the processor's architecture (32 or 64 bits).
    2. Using python's pip installer, install the following dependecies:
        matplotlib, requests, PyQt5, python-osc, python-vlc 
        Example: 
        pip install requests
        pip install PyQt5
    3. Open "SC_Conf.json" and edit the following fields ONLY. 
        3.1 Set "metadata_folder" and "vlc_snapshots_folder" to a local folder in the target computer (ex: "C:/Users/User/Documents/sister_city_media/")
        3.2 Set "ableton_session_filepath" to the location of the Ableton Live session.
    4. On a terminal, use the command “cd” to move to the project folder (where the file "SC_Main.py" is located)
    5. Run “python SC_Main.py”
