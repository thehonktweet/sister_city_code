
import argparse
import math
import threading
import SC_image_analyzer
import sys

from pythonosc import dispatcher
from pythonosc import osc_server
import os
import json
import time
import pyautogui

_image_analyzer_counter = 0
_ableton_counter = 0

_max_image_analyzer_counter = 30
_max_ableton_counter = 30
_ableton_session_filepath = ""
_osc_watchdog_port = 0
_ableton_fail_attempts = 0
_max_ableton_fail_attempts = 3
_ableton_launch_waiting_time = 40

def loadConfiguration():

    global _osc_watchdog_port, _ableton_session_filepath

    try:
        
        current_file_dir = os.path.dirname(os.path.realpath(__file__))
        other_file_path = os.path.join(current_file_dir, "SC_Conf.json")
        #dir_path = os.path.dirname(os.path.realpath(__file__))
        
        #print("other file: " + dir_path)
        
        with open(other_file_path, 'r') as read_file:
            conf = json.load(read_file)
            _osc_watchdog_port = conf["osc_watchdog_port"]
            _ableton_session_filepath = conf["ableton_session_filepath"]
            
            
    except Exception as e:
        print("Unable to load configuration file")
        print(e)
        sys.exit()

def startAbleton():
    global _ableton_session_filepath, _ableton_launch_waiting_time
    
    os.startfile(_ableton_session_filepath)
    time.sleep(_ableton_launch_waiting_time)
    
    #bypass auto save window
    print("moving mouse to 560x406")
    pyautogui.moveTo(560, 406)  # use tweening/easing function to move mouse over 2 seconds.
    
    print("first click")
    pyautogui.click()
    time.sleep(1)
    print("second click")
    pyautogui.click()

def stopAbleton():
    os.system('TASKKILL /F /IM "Ableton Live 10 Suite.exe"')
    time.sleep(2)

def imageAnalyzerAlive(unused_addr, args, trigger):
    global _image_analyzer_counter
    #print("resetting imageAnalyzer counter")
    _image_analyzer_counter = 0
def abletonAlive(unused_addr, args, trigger):
    global _ableton_counter
    #print("resetting ableton counter")
    _ableton_counter = 0

def abletonWantsRestart(unused_addr, args, trigger):
    restartComputer()

def restartComputer():
    os.system("shutdown /r /t 1")

loadConfiguration()
startAbleton()

dispatcher = dispatcher.Dispatcher()
dispatcher.map("/abletonAlive", abletonAlive, "trigger")
dispatcher.map("/imageAnalyzerAlive", imageAnalyzerAlive, "trigger")
dispatcher.map("/abletonWantsRestart", abletonWantsRestart, "trigger")

server = osc_server.ThreadingOSCUDPServer(("127.0.0.1", _osc_watchdog_port), dispatcher)

threading.Thread(target=server.serve_forever).start()

print("Sister City Image Analyzer initiated")

t = threading.Thread(target=SC_image_analyzer.main)
t.daemon = True
t.start()

while True:

    if(_ableton_counter == _max_ableton_counter ):
    
        if (_ableton_fail_attempts == _max_ableton_fail_attempts):
            restartComputer()
        else:
            stopAbleton()
            startAbleton()
            _ableton_fail_attempts
            _ableton_counter = 0
    
    elif(_image_analyzer_counter == _max_image_analyzer_counter):
        restartComputer()
    else:
        _ableton_counter += 1
        _image_analyzer_counter += 1
        
        time.sleep(1)
 
