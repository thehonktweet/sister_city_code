#! /usr/bin/python

#
# Qt example for VLC Python bindings
# Copyright (C) 2009-2010 the VideoLAN team
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
#

import sys
import os.path
from PyQt5.QtCore import Qt, QTimer
from PyQt5.QtGui import QPalette, QColor
from PyQt5.QtWidgets import QMainWindow, QWidget, QFrame, QSlider, QHBoxLayout, QPushButton, \
    QVBoxLayout, QAction, QFileDialog, QApplication
import vlc
import datetime
import time
import os

# Cognitive Services Imports
import json
import requests
# If you are using a Jupyter notebook, uncomment the following line.
# %matplotlib inline
import matplotlib.pyplot as plt
from io import BytesIO
import io
from pythonosc import osc_message_builder
from pythonosc import udp_client
import argparse
import threading

_debug = False

# examples of _external_camera_input
# rtsp://admin:Wearelisten2018@192.168.0.105:554/cam/realmonitor?channel=1&subtype=1
# iSight camera: "avcapture://CC25253FQ9AG1HNAX"

_microsoft_subscription_key = ""
_time_between_snapshots = ""
_vlc_snapshots_folder = ""
_metadata_folder = ""
_tagsToListenFor = []
_ts = time.time()

# snapshot analysis
_json_log = ""
_analyze_url = ""
## Picture mode "True" set the script to only take pictures. Images won't be analyzed or deleted.
_picture_only_mode = False

#_delete_analyzed_pics = True deletes all images after being send out to the cloud for analysis.
_delete_analyzed_pics = True
_export_analyses = False

# OSC Server Configuration 
_osc_host_port = 0
_osc_host_ip = "" 

_max_days_meta_retention = 0
_probability_threshold = 0

#Adding multithreading

class functions():
	
	def sendOSCMessage(self, client_ip, client_port, type, msg):
    	
		try:
			parser = argparse.ArgumentParser()
			parser.add_argument("--ip", default= client_ip, help="The ip of the OSC server")
			parser.add_argument("--port", type=int, default= client_port, help="The port the OSC server is listening on")
			args = parser.parse_args()

			client = udp_client.SimpleUDPClient(args.ip, args.port)

			client.send_message(type, msg)
		
		except Exception as e:
			print("Unable to connect to client via OSC")
			print(e)	
    
	def getTimeStamp(self):
        
		ts = time.time()
		return datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H-%M-%S')
	
	def tsOlderThanXDays(self, theTS, numDays):
		try:
			ts = time.time() #now
			t1 = datetime.datetime.strptime(theTS, '%Y-%m-%d-%H-%M-%S')
		
			#timeSum = datetime.datetime.fromtimestamp(ts) + datetime.timedelta(days=numDays)

			timeSum = t1 + datetime.timedelta(days=numDays)
		
			#print("theTS: " + theTS + ", timeSum: " + str(timeSum) + ", " + "t1: " + str(datetime.datetime.fromtimestamp(ts)))
		
			if(timeSum < datetime.datetime.fromtimestamp(ts)):
				return True
			return False
		
		except Exception as e:
			print("Error dealing with date types")
			print(e)
			sys.exit()
        
    
def loadConfiguration():

	global _microsoft_subscription_key, _time_between_snapshots, \
	_vlc_snapshots_folder, _metadata_folder, _tagsToListenFor, \
	_picture_only_mode, _delete_analyzed_pics, _max_days_meta_retention, _osc_host_port, _osc_host_ip, \
	_osc_watchdog_port, _analyze_url, _sc_camera_url, _probability_threshold 

	try:
        
		current_file_dir =  os.path.dirname(os.path.realpath(__file__))
		other_file_path = os.path.join(current_file_dir, "SC_Conf.json")
    
		with open(other_file_path, 'r') as read_file:
			conf = json.load(read_file)
			_microsoft_subscription_key = conf["microsoft_subscription_key"]
			_analyze_url = conf["microsoft_api_url"]
			_time_between_snapshots = int(conf["time_between_snapshots"])
			_vlc_snapshots_folder = conf["vlc_snapshots_folder"]
			
			_metadata_folder = conf["metadata_folder"]
			_tagsToListenFor = conf["tagsToListenFor"]
			
			_sc_camera_url  = conf["camera_url"]
			_picture_only_mode = conf["picture_only_mode"]
			_delete_analyzed_pics = conf["delete_analyzed_pics"]
			_max_days_meta_retention  = conf["max_days_meta_retention"]

			_osc_watchdog_port = conf["osc_watchdog_port"]
			_osc_host_port = conf["osc_host_port"]
			_osc_host_ip = conf["osc_host_ip"]
			_probability_threshold = conf["probability_threshold"]
            
	except Exception as e:
		print("Unable to load configuration file")
		print(e)
		sys.exit()

def initJSONLog():

	global _json_log, _metadata_folder
	data = """ 
    {
		'analysis' : {
			'tags' : [
				{
					'name' : 'nothing',
					'timestamp' : [  '""" + functions().getTimeStamp() + """']
				}
			]
		}
	}
	"""
	_json_log = json.loads(data.replace("'", '"'))
	try:
		with open(_metadata_folder + "SC_meta_log" + ".json", 'r') as read_file:
			_json_log = json.load(read_file)

	except:
		with open(_metadata_folder + "SC_meta_log" + ".json", 'w') as outfile:
			json.dump(_json_log, outfile)
            
def updateJSONLog():
	global _json_log, _metadata_folder
    
	try:
		to_unicode = unicode
	except NameError:
		to_unicode = str
            
	with io.open(_metadata_folder + "SC_meta_log" + ".json", 'w', encoding='utf8') as outfile:
		str_ = json.dumps(_json_log,
                            indent=4, sort_keys=True,
                            separators=(',', ': '), ensure_ascii=False)
		outfile.write(to_unicode(str_))
        
def init():

	loadConfiguration()
	initJSONLog()

def main():
	
	app = QApplication(sys.argv)
    
	init()
    
	player = Player()
	player.show()
	player.resize(640, 480)
	player.OpenFile()

	sys.exit(app.exec_())


# You must use the same region in your REST call as you used to get your
# subscription keys. For example, if you got your subscription keys from
# westus, replace "westcentralus" in the URI below with "westus".
#
# Free trial subscription keys are generated in the westcentralus region.
# If you use a free trial subscription key, you shouldn't need to change
# this region.

class Player(QMainWindow):
	"""A simple Media Player using VLC and Qt
	"""

	def __init__(self, master=None):

		QMainWindow.__init__(self, master)
		self.setWindowTitle("Media Player")

		# creating a basic vlc instance
		self.instance = vlc.Instance()
		# creating an empty vlc media player
		self.mediaplayer = self.instance.media_player_new()

		self.createUI()
		self.isPaused = False

	def createUI(self):
		"""Set up the user interface, signals & slots
		"""
		self.widget = QWidget(self)
		self.setCentralWidget(self.widget)

		# In this widget, the video will be drawn
		if sys.platform == "darwin":  # for MacOS
			from PyQt5.QtWidgets import QMacCocoaViewContainer
			self.videoframe = QMacCocoaViewContainer(0)
		else:
			self.videoframe = QFrame()
		self.palette = self.videoframe.palette()
		self.palette.setColor(QPalette.Window,
                              QColor(0, 0, 0))
		self.videoframe.setPalette(self.palette)
		self.videoframe.setAutoFillBackground(True)
        
		self.hbuttonbox = QHBoxLayout()
		self.playbutton = QPushButton("Play")
		#self.hbuttonbox.addWidget(self.playbutton)
		self.playbutton.clicked.connect(self.PlayPause)

		self.stopbutton = QPushButton("Stop")
		#self.hbuttonbox.addWidget(self.stopbutton)
		self.stopbutton.clicked.connect(self.Stop)

		self.vboxlayout = QVBoxLayout()
		self.vboxlayout.addWidget(self.videoframe)

		self.widget.setLayout(self.vboxlayout)

		open = QAction("&Open", self)
		open.triggered.connect(self.OpenFile)
		exit = QAction("&Exit", self)
		exit.triggered.connect(sys.exit)
		menubar = self.menuBar()
		filemenu = menubar.addMenu("&File")
		filemenu.addAction(open)
		filemenu.addSeparator()
		filemenu.addAction(exit)


		self.timer = QTimer(self)
		self.timer.setInterval(_time_between_snapshots * 1000)
		self.timer	.timeout.connect(self.updateUI)

	def PlayPause(self):
		"""Toggle play/pause status
		"""
		if self.mediaplayer.is_playing():
			self.mediaplayer.pause()
			self.playbutton.setText("Play")
			self.isPaused = True
		else:
			if self.mediaplayer.play() == -1:
				self.OpenFile()
				return
			self.mediaplayer.play()
			self.playbutton.setText("Pause")
			self.timer.start()
			self.isPaused = False

	def Stop(self):
		"""Stop player
		"""
		self.mediaplayer.stop()
		self.playbutton.setText("Play")

	def OpenFile(self, filename=None):
		"""Open a media file in a MediaPlayer
		"""
		global _sc_camera_url

		if _debug: 
			print("setting camera url to: " + _sc_camera_url)
            
		self.media = self.instance.media_new(_sc_camera_url)
		
		self.media.get_mrl()
		# put the media in the media player
		self.mediaplayer.set_media(self.media)

		self.setWindowTitle("SisterCity Camera Stream")

		# the media player has to be 'connected' to the QFrame
		# (otherwise a video would be displayed in it's own window)
		# this is platform specific!
		# you have to give the id of the QFrame (or similar object) to
		# vlc, different platforms have different functions for this
		if sys.platform.startswith('linux'):  # for Linux using the X Server
			self.mediaplayer.set_xwindow(self.videoframe.winId())
		elif sys.platform == "win32":  # for Windows
			self.mediaplayer.set_hwnd(self.videoframe.winId())
		elif sys.platform == "darwin":  # for MacOS
			self.mediaplayer.set_nsobject(int(self.videoframe.winId()))
		self.PlayPause()

	def setVolume(self, Volume):
		"""Set the volume
		"""
		self.mediaplayer.audio_set_volume(Volume)

	def setPosition(self, position):
		"""Set the position
		"""
		# setting the position to where the slider was dragged
		self.mediaplayer.set_position(position / 1000.0)
		# the vlc MediaPlayer needs a float value between 0 and 1, Qt
		# uses integer variables, so you need a factor; the higher the
		# factor, the more precise are the results
		# (1000 should be enough)

	def updateUI(self):
        
		global _vlc_snapshots_folder, _picture_only_mode
        
		"""updates the user interface"""
		# setting the slider to the desired position
		#self.positionslider.setValue(self.mediaplayer.get_position() * 1000)

		if self.mediaplayer.is_playing():

			pictureName = functions().getTimeStamp()
			self.mediaplayer.video_take_snapshot(
                0, _vlc_snapshots_folder + pictureName + ".png", 0, 0)
                
			try:
				if _picture_only_mode == False:
					imageAnalysis(pictureName).start()
                
			except Exception as e: 
				print("Error while updating UI")  
				print(e)      

		else:
			# no need to call this function if nothing is played
			self.timer.stop()
			self.OpenFile()

			# if not self.isPaused:
			# after the video finished, the play button stills shows
			# "Pause", not the desired behavior of a media player
			# this will fix it
			# self.Stop()     
    
class imageAnalysis (threading.Thread):

	def __init__(self, pictureName):
		threading.Thread.__init__(self)
		self.pictureName = pictureName
     
	def run(self):
		self.analyzeImage(self.pictureName)
      
	def analyzeImage(self, pictureName):

		global _debug, _vlc_snapshots_folder, _microsoft_subscription_key, _analyze_url, _osc_watchdog_port, _osc_host_ip
		
		if _debug:
			print("")
			print("New Thread started, attempting to sleep")
			time.sleep(1)
			print("waking up")
			time.sleep(1)
		if _debug:
			print("")
			print("*** NEW ANALYSIS ***")
			print("snapshot folder: " + _vlc_snapshots_folder) 
			print("subscription key: " + _microsoft_subscription_key)
			print("api_url: " + _analyze_url)
			print("Image's filepath: " + _vlc_snapshots_folder + pictureName + ".png")
			print("OSC watchdog port: " + str(_osc_watchdog_port))
        	
		#report to watchdog
		functions().sendOSCMessage(_osc_host_ip, _osc_watchdog_port, "/imageAnalyzerAlive", 1)
        # Read the image into a byte array
		image_data = open(_vlc_snapshots_folder +
                              pictureName + ".png", "rb").read()
		headers = {'Prediction-Key': _microsoft_subscription_key,
                       'Content-Type': 'application/octet-stream'}
		
		response = requests.post(
            _analyze_url, headers=headers, data=image_data)
		response.raise_for_status()

		# The 'analysis' object contains various fields that describe the image. The most
		# relevant caption for the image is obtained from the 'description' property.
		analysis = response.json()
		if _debug:
			print("")
			print(analysis)

		#functions().sendOSCMessage(_osc_host_ip, _osc_host_port, "color", analysis["color"]["accentColor"])
        
		self.loadJSONFile(pictureName, analysis)
		if _delete_analyzed_pics:
			#time.sleep(3)
			os.remove(_vlc_snapshots_folder +
                              pictureName + ".png")
			if _debug:
				print("Picture removed!")
    
	def loadJSONFile(self, pictureName, data):
		#CHECK
		global _json_log, _osc_host_ip, _osc_host_port, _probability_threshold
		
		p = []
		tag = ""
        
		try:

			ts = functions().getTimeStamp()
            
			# tags already existing in the log
			loggedTags = _json_log["analysis"]["tags"]
			#print(loggedTags)
            
			tag_str = ""
			for p in data["predictions"]:
				tag = p["tagName"]
				if self.tagIsOfInterest(tag) and p["probability"] >= _probability_threshold:
                    
					if(len(tag_str) > 0):
						tag_str += ", " + tag
					else:
						tag_str += tag
					if _export_analyses:	
						self.exportPictureAnalysis(pictureName, data)
					if not self.tagIsInLog(tag, loggedTags):
						_json_log["analysis"]["tags"].append(
                            {"name": tag, "timestamp": [ts]})
					else:
						for v in _json_log["analysis"]["tags"]:
							if(v["name"] == tag):
								v["timestamp"].append(ts)

			if len(tag_str) == 0:   
				_json_log["analysis"]["tags"][0]["timestamp"].append(ts)
				functions().sendOSCMessage(_osc_host_ip, _osc_host_port, "/tag", "none")
			else:
				tags_array = tag_str.split(",")

				for t in tags_array:
					functions().sendOSCMessage(_osc_host_ip, _osc_host_port, "/tag/" + t.strip(), " ")

			# Write JSON file
			self.updateJSONLog()

			self.clearOldMetadata()        

		except Exception as e:
			print("Error while parsing the analysis of picture '" + pictureName + "'")
			print(e)

	def clearOldMetadata(self):
		global _max_days_meta_retention, _json_log

		try:
	
			for v in _json_log["analysis"]["tags"]:
				tArray = []
				for t in v["timestamp"]:
					if not functions().tsOlderThanXDays(t, _max_days_meta_retention):
						tArray.append(t)
				if(len(tArray) == 0):
					del v
				else:
					v["timestamp"] = []
					v["timestamp"] = tArray
		
			self.updateJSONLog() 	
		except Exception as e:
			print("Error while clearing old metadata")
			print(e)	
		
	def exportPictureAnalysis(self, pictureName, analysis):
  
		global _metadata_folder
        
		try:
			to_unicode = unicode
		except NameError:
			to_unicode = str
            
		with io.open(_metadata_folder + pictureName + ".json", 'w', encoding='utf8') as outfile:
			str_ = json.dumps(analysis,
                                  indent=4, sort_keys=True,
                                  separators=(',', ': '), ensure_ascii=False)
			outfile.write(to_unicode(str_))
                
	def tagIsInLog(self, newTag, loggedTags):

		for t in loggedTags:
			if(t["name"] == newTag):
				return True

		return False

	def tagIsOfInterest(self, tag):
		global _tagsToListenFor

		for t in _tagsToListenFor:
			if(tag == t):
				return True
		return False        
    
	def updateJSONLog(self):
		global _json_log, _metadata_folder

		try:
			to_unicode = unicode
		except NameError:
			to_unicode = str
   
		with io.open(_metadata_folder + "SC_meta_log" + ".json", 'w', encoding='utf8') as outfile:
			str_ = json.dumps(_json_log,
                            indent=4, sort_keys=True,
                            separators=(',', ': '), ensure_ascii=False)
			outfile.write(to_unicode(str_))

	

